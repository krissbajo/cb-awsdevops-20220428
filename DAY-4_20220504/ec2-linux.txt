#!/bin/bash

# Launch AWS EC2 Instance Of Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-0cfedf42e63bba657" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-5183ac39" \
--security-group-ids "sg-0bff0e94dff49008a" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Linux-WebServer},{Key=Environment,Value=Dev},{Key=Type,Value=WebServer}]' \
--key-name "mumbai_keys" --profile SuperHero
#--user-data file://install-utilities.txt --profile SuperHero
