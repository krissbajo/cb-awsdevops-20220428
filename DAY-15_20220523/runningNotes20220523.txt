Agenda :

1. Linux / Unix Text Editor : VI or VIM 

2. Packer & Terraform

3. Migration Strategies & Deployment Models

4. AWS DevOps Tools(CodeCommit, CodeBuild, CodePipeline)

Completed:
5. End To End Real Time Project with Java & Maven using Git, GitHub, Jenkins, Jenkins Pipeline, Sonarqube, Jfrog, & Tomcat

AWS:
6. End To End Real Time Project with Java & Maven using Git, CodeCommit, CodeBuild, CodePipeline, IAM, S3, VPC, EC2

Q?
