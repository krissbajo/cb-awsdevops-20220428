# versions.tf
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider - provider.tf
provider "aws" {
  region  = "ap-south-1"
  profile = "default"
}

# Provision EC2 Instance  - main.tf 
resource "aws_instance" "linux" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids
  key_name               = var.key_name
  user_data              = file("web.sh")
  tags = {
    Name        = var.name
    Environment = "Development"
    ProjectName = "CloudBinary"
    ProjectID   = "20211204"
    CreatedBy   = "Iac-Terraform"
  }
}

# variables.tf 

# Outputs.tf
output "instance_id" {
  description = "Print Instance ID"
  value       = aws_instance.linux.id
}

output "instance_public_ip" {
  description = "Print Public IP"
  value       = aws_instance.linux.public_ip
}

