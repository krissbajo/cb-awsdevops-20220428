variable "ami" {
  default = "ami-05ba3a39a75be1ec4"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "subnet_id" {
  default = "subnet-737ce03f"
}
variable "vpc_security_group_ids" {
  default = ["sg-0bff0e94dff49008a"]
}
variable "key_name" {
  default = "mumbai_keys"
}
variable "name" {
  default = "CloudBinary-Linux"
}