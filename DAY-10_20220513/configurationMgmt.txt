Configuration Automation / Configuration Managment :

Configuration Management(Doing System Administration Work):
    - OS :
        - Unix/Linux/MacOS 
        - Windows
    - Administration & Networking
        - Configuration Management Tools : 1.Puppet, 2.Chef, 3.Saltstack & 4.Ansible 
            Types Of Tools :
                - Pull Based : 1.Puppet, 2. Chef [Server Software and Client Software]
                - Push Based :  3. Saltstack & 4. Ansible [Agentless]

Puppet :
Puppet is an open source configuration management utility. 
It runs on many Unix-like systems as well as on Microsoft Windows and includes its own declarative language to describe 
system configuration. The current version is Puppet 4.

Chef:
Chef is a configuration management tool used to streamline the task of configuring and maintaining a company's 
servers and can integrate with cloud-based platforms such as Rackspace Internap Amazon EC2 Google Cloud Platform 
OpenStack SoftLayer and Microsoft Azure to automatically provision and configure new machines. 
Chef contains solutions for both small and large scale systems with features and pricing for the respective ranges.

Saltstack :
Salt platform or SaltStack is a Python-based open source configuration management and remote execution application. 
Supporting the "infrastructure-as-code" approach to deployment and cloud management it competes 
primarily with Puppet Chef and Ansible.

Ansible :
Ansible is an open-source software platform for configuring and managing computers. 
It combines multi-node software deployment ad hoc task execution and configuration management. 
It manages nodes over SSH or PowerShell and requires Python (2.4 or later) to be installed on them. 
Modules work over JSON and standard output and can be written in any programming language. 
The system uses YAML to express reusable descriptions of systems.

