#!/bin/bash

# Create a Linux Operating System using AWS CLI Commands
aws ec2 run-instances \
--image-id "ami-005de95e8ff495156" \
--instance-type "t2.large" \
--count 1 \
--subnet-id "subnet-2db46060" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Jfrog},{Key=Environment,Value=Development},{Key=Project Name,Value=CloudBinary},{Key=Project ID,Value=20211204},{Key=Email ID,Value=contact.cloudbinary@gmail.com},{Key=Mobile Number,Value=+91 6309872424}]'  \
--security-group-ids  "sg-0d25228d01ad4f02a"   \
--key-name "cb_aws_nv_keys" \
--user-data file://install-jfrog.txt