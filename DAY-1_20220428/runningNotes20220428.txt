Agenda :

Getting Started with AWS DevOps

Setting Up the Lab:
Prerequisites Download, Install and Configure :

1. IDE : VSCode
SCM / VCS CLI Tool : Git 
SCM / VCS GUI Tools : GitHub, GitLab, & BitBucket
Cloud Computing Account : AWS
AWS CLI 2 : https://awscli.amazonaws.com/AWSCLIV2.msi

IAM User Creation:
https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html

What is DevOps?

DevOps is a set of practices that combines software development and IT operations. It aims to shorten the systems development life cycle and provide continuous delivery with high software quality. DevOps is complementary with Agile software development; several DevOps aspects came from the Agile methodology. 

A compound of development (Dev) and operations (Ops), DevOps is the union of people, process, and technology to continually provide value to customers.

What does DevOps mean for teams? DevOps enables formerly siloed roles—development, IT operations, quality engineering, and security—to coordinate and collaborate to produce better, more reliable products. By adopting a DevOps culture along with DevOps practices and tools, teams gain the ability to better respond to customer needs, increase confidence in the applications they build, and achieve business goals faster.


What is Agile?

able to move quickly and easily.

In software development, agile practices include requirements discovery and solutions improvement through the collaborative effort of self-organizing and cross-functional teams with their customer/end

The Agile methodology is a way to manage a project by breaking it up into several phases. It involves constant collaboration with stakeholders and continuous improvement at every stage. Once the work begins, teams cycle through a process of planning, executing, and evaluating.

https://en.wikipedia.org/wiki/Scrum_(software_development)

Team:
4	Scrum team
4.1	Product owner
4.2	Developers
4.3	Scrum master

Workflow:
5.1	Sprint
5.2	Sprint planning
5.3	Daily scrum
5.4	Sprint review
5.5	Sprint retrospective
5.6	Backlog refinement
5.7	End & Start a sprint

Artifacts:
6.1	Product backlog
6.2	Sprint backlog
6.3	Increment


Roles and Responsibilities of DevOps Engineer?
1. Monitoring : NewRelic
2. Incident Managment : ITSM Tool ( ServiceNow )
3. System Analysis 
4. Troubleshooting 
5. Infrastructure Provisioning : AWS CF, Terraform, AZURE ARM T etc...
6. Handling of Ad-hoc requests
7. Build & Deployments
8. CI/CD Pipelines
9. Patch Activites
10. Security Vulnerabilities 
11. Automation : ShellScript, Powershell, DevOps Tools, Python etc..
12. Documentation : Sharepoint, Confluence 
13. Cost Optimation 

To Become DevOps Engineer?

1. SDLC:
2. Administration & Networking
3. Scripting
4. Programming
5. RDBMS / NoSQL
6. VCS / SCM 
7. CI
8. CB / CM : Jira, MS Teams,
9. CSCA Tools 
10. CBCR
11. CM Tools
12. IaC : 
13. Containerise : 
14. Container / Cluster Orchestration 
15. CM

Operating System Basics : Linux 

1. Unix : Distribtions

Flavors that are available commercially (read: sold) include:

Solaris – Sun Microsystems’ implementation, of which there are different kinds available: these are Solaris OS for SPARC platforms, Solaris OS for x86 platforms, and Trusted Solaris for both SPARC & x86 platforms; the latest version is Solaris 10 OS

AIX – short for Advanced Interactive eXecutive; IBM’s implementation, the latest release of which, is the AIX 5L version 5.2.

SCO UnixWare and OpenServer – are implementations derived from the original AT&T Unix® source code acquired by the Santa Cruz Operation Inc. from Novell, and later on bought by Caldera Systems; the latest versions are UnixWare 7.1.3 and OpenServer 5.0.7

BSD/OS – the Berkeley Software Distribution (BSD) Unix implementation from Wind River; its latest version is the BSD/OS 5.1 Internet Server Edition

IRIX – the proprietary version of Unix from Silicon Graphics Inc.; the latest release of which is IRIX 6.5

HP-UX – short for Hewlett-Packard UniX; the latest version is the HP-UX 11i

Tru64 UNIX – the Unix operating environment for HP AlphaServer systems; Tru64 UNIX v5.1B-1 is the latest version

Mac OS – Mac operating system from Apple Computer Inc. having a Unix core; the latest version is the Mac OS X Panther

Flavors that are available for free, include:

FreeBSD – derived from BSD, it is an advanced OS for x86 compatible AMD64, Alpha, IA-64, PC-98 and UltraSPARC® architectures; the latest versions are FreeBSD 5.2.1 (New Technology Release) and the FreeBSD 4.9 (Production Release)

NetBSD – Unix-like OS derived from BSD and developed by The NetBSD Project; it is shipped under a BSD license and the latest release is NetBSD 1.6.2

OpenBSD – multi-platform 4.4BSD-based Unix-like OS from The OpenBSD project; its latest release is OpenBSD 3.4

Linux — a Unix-type OS originally created by Linus Torvalds, the source code of which is available freely and open for development under GNU General Public License; there are numerous Linux distributions available


2. MacOS : 

3. Microsoft  :
  - DOS : CUI/CLI 
  - Windows : GUI + CUI/CLI

4. Linux Distributions 
  
1 Ubuntu
2 Debian
3 CentOS
4 Red Hat Enterprise Linux (RHEL)
5 Gentoo
6 Fedora
7 OpenSUSE
8 Scientific Linux
9 CloudLinux
10 Elementary OS
11 Linux Mint
12 Arch Linux
13 Manjaro
14 Oracle Linux
15 Slackware
16 Mageia
17 Clear Linux
18 Rocky Linux
19 AlmaLinux
20 Asahi Linux
21 Lubuntu
22 SUSE Linux
23 Knoppix
24 VzLinux
25 Peppermint OS
26 Zorin OS
27 BlackArch Linux


Q?

