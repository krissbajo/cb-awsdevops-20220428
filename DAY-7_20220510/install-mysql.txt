#!/bin/bash

# Setup Hostname 
sudo hostnamectl set-hostname "db.cloudbinary.io"

# Update the hostname part of Host File
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Update Ubuntu Repository 
sudo apt-get update 

# Download, & Install Utility Softwares 
sudo apt-get install git wget unzip curl tree -y 

# Download, Install & Configure Web Server i.e. Apache2 
sudo apt-get install mysql-server -y 

# Azure@DevOps123456




# To Restart SSM Agent on Ubuntu 
sudo systemctl restart snap.amazon-ssm-agent.amazon-ssm-agent.service

# Attach Instance profile To EC2 Instance 
# aws ec2 associate-iam-instance-profile --iam-instance-profile Name=SA-EC2-SSM --instance-id ""

