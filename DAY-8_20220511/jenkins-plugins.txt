Jenkins :
    - Java Variable Configuration on Server and UI 
    - Maven Variable Configuration on Server and UI 
    - Plugins
    - Creating Jenkins Jobs on UI  :

Maven  LifeCycles :
https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html

        - Validate
        - Clean 
        - Compile
        - test
        - package
        - verify
        - install
        - deploy
        - release

List of Plugins :
https://plugins.jenkins.io/conditional-buildstep/
https://plugins.jenkins.io/deploy/
https://plugins.jenkins.io/envinject/
https://plugins.jenkins.io/git-parameter/
https://plugins.jenkins.io/github-branch-pr-change-filter/
https://plugins.jenkins.io/gitlab-branch-source/
https://plugins.jenkins.io/maven-plugin/
https://plugins.jenkins.io/maven-invoker-plugin/
https://plugins.jenkins.io/publish-over-ssh/
